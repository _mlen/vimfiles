set nocompatible

" Load plugins
execute pathogen#infect()

" Settings
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set textwidth=79
set backspace=indent

set modeline
set ruler
set number
set showmatch
set title

set backup
set backupcopy=yes
set ignorecase
set smartcase
set nowrap
set wildmenu
set wildignore+=*.swp,*~,.git,.svn,*/vendor/bundle/*,*/node_modules/*,*/dist/*,*/tmp/*
set completeopt=menu,longest,preview
set foldmethod=syntax
set viewoptions=folds,cursor
set smarttab
set fo+=jc
set fo-=t
set cinoptions+=g0N-s

if version >= 703
  set undofile
end

if has('mouse')
  set mouse=a
  set mousehide
end

if (&t_Co > 2) || has("gui_running")
  syntax on
  set hlsearch
  set cursorline
  set background=dark

  if $TMUX == ''
    set clipboard=unnamed

    if has('unnamedplus')
      set clipboard+=unnamedplus
    end
  end

  if !empty(globpath(&rtp, "colors/hybrid.vim"))
    colorscheme hybrid
  else
    colorscheme slate
  end

  set listchars=tab:▸\ ,eol:¬,nbsp:˽,trail:˰
end

if has("autocmd")
  filetype plugin indent on
  set omnifunc=syntaxcomplete#Complete

  autocmd FileType c,cpp,java,objc,markdown,python,rst setlocal ts=4 sts=4 sw=4
  autocmd FileType tex,text,mail,rst,markdown,gitcommit setlocal spell spelllang=pl,en fo+=awn1t

  autocmd FileType mail,gitcommit setlocal fileencoding=utf8 textwidth=72 fo+=awn1t

  autocmd FileType c,cpp nmap <leader>i :ClangFormat<cr>
  autocmd FileType c,cpp nmap <leader>s :ClangSyntaxCheck<cr>

  autocmd FileType ruby nmap <leader>r :call RunRspec()<cr>

  autocmd FileType python setlocal omnifunc=jedi#completions

  autocmd FileType tex inoremap <buffer> <Space> <C-R>=TeXSpace()<CR>

  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
else
  set autoindent
  set smartindent
end

" Functions
function! MoveFile()
  let old_name = expand('%')
  let new_name = input('New file name: ', expand('%'))
  if new_name != '' && new_name != old_name
    exec ':saveas ' . new_name
    exec ':silent !rm ' . old_name
    redraw!
  endif
endfunction

function! SearchMultiLine(bang, ...)
  if a:0 > 0
    let sep = (a:bang) ? '\_W\+' : '\_s\+'
    let @/ = join(a:000, sep)
  endif
endfunction

function! TeXSpace()
  let place = strpart(getline(line('.')), col('.') - 3, 2)
  if place =~? '\v^(\s|\~)?[aiouwz]$'
    return '~'
  endif
  return ' '
endfunction

function! InsertTabWrapper()
  let col = col('.') - 1
  if pumvisible()
    return "\<c-n>"
  elseif !col || getline('.')[col - 1] !~ '\S'
    return "\<tab>"
  else
    return "\<c-x>\<c-o>"
  end
endfunction

function! RunRspec()
  execute "!b rspec ".expand("%").":".line(".")
endfunction

" Commands
command! StripWhitespace %s/\s\+$//e|let @/=""
command! -bang -nargs=* -complete=tag S call SearchMultiLine(<bang>0, <f-args>)|normal! /<C-R>/<CR>

" Remaps
imap <tab>   <c-r>=InsertTabWrapper()<cr>
imap <s-tab> <c-p>

nmap <leader>i gg=G``
nmap <leader>m :call MoveFile()<cr>
nmap <leader>e :Explore<cr>
nmap <leader>a :Ack '\b<c-r><c-w>\b'<cr>

" Split navigation
nmap <C-h> <C-w>h
nmap <C-k> <C-w>k
nmap <C-j> <C-w>j
nmap <C-l> <C-w>l

" Shift code
noremap  <tab>   v>
noremap  <s-tab> v<
vnoremap <tab>   >gv
vnoremap <s-tab> <gv

" Color overrides for vim2hs
hi! link Conceal Operator

" Plugin settings
let NERDBlockComIgnoreEmpty = 0
let NERDCommentWholeLinesInVMode = 2
let NERDSpaceDelims = 1
let NERDRemoveExtraSpaces = 1

let g:jedi#auto_vim_configuration = 0
let g:jedi#popup_on_dot = 0
let g:jedi#use_tabs_not_buffers = 0
let g:jedi#smart_auto_mappings = 0

let g:tern_show_signature_in_pum = 1

let g:clang_auto = 0
let g:clang_compilation_database = '.'
let g:clang_format_style = 'file'

let g:clang_c_completeopt = &completeopt
let g:clang_cpp_completeopt = &completeopt
let g:clang_cpp_options = '-Wall -std=c++14 -stdlib=libc++'
let g:clang_c_options = '-Wall -std=c11'

let erlang_show_errors = 0

let g:haskell_conceal = 0
let g:haskell_tabular = 0

let g:markdown_fenced_languages = ['coffee', 'css', 'javascript',
      \'js=javascript', 'json=javascript', 'ruby', 'python', 'xml', 'html',
      \'haskell', 'c', 'cpp', 'go', 'dtrace', 'erlang', 'idris', 'vim']
